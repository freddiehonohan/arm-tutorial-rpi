#include <stdio.h>

int main (void)
{
	// Remove the follow __asm line to check if you're using semihosting.
        // Throws a link-time error if semihosting is being used.
        //__asm(".global __use_no_semihosting\n\t");
	printf("hello world\n");
	return 0;
}
