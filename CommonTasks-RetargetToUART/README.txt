===========================================
README
===========================================

Overview
-------------------------------------------
The files in this package accompany the "Retarget Embedded Output to UART" guide:

	https://developer.arm.com/common-tasks-retarget-embedded-output-to-uart

Please refer to the above guide for full instructions.

Quick Reference Build Instructions
-------------------------------------------

Configure your Arm DS environment to run Arm Compiler 6 from the command-line:

https://developer.arm.com/docs/101470/1900/configure-arm-development-studio/register-a-compiler-toolchain/configure-a-compiler-toolchain-for-the-arm-ds-command-prompt

1-AboutSemihosting
	armclang -c -g --target=aarch64-arm-none-eabi hello_world.c
	armclang -c -g --target=aarch64-arm-none-eabi startup.s
	armlink --scatter=scatter.txt --entry=start64 hello_world.o startup.o
	fromelf --text -c __image.axf --output=disasm.txt

	<remove commented line in main.c>
	armclang -c -g --target=aarch64-arm-none-eabi hello_world.c
	armclang -c -g --target=aarch64-arm-none-eabi startup.s
	armlink --scatter=scatter.txt --entry=start64 hello_world.o startup.o
	<Error: L6915E: Library reports error: __use_no_semihosting was requested> 

2-RetargetFunctionsToUseUART
	armclang -c -g --target=aarch64-arm-none-eabi hello_world.c
	armclang -c -g --target=aarch64-arm-none-eabi startup.s
	armclang -c -g --target=aarch64-arm-none-eabi pl011_uart.c
	armlink --scatter=scatter.txt --entry=start64 hello_world.o startup.o pl011_uart.o
	fromelf --text -c __image.axf --output=disasm.txt

	Now follow the instructions in the "Retarget Embedded Output to UART" guide to run this image using Telnet from within Arm DS.

